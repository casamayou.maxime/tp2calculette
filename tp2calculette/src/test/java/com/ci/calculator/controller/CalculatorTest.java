package com.ci.calculator.controller;

import java.util.Date;

import org.junit.*;

import com.ci.calculator.model.History;
import com.ci.calculator.model.OperationText;

public class CalculatorTest 
{
	private Calculator maCal;
	private History myHistory;
	
	@Before
	public void createCalculator() 
	{
		this.maCal = new Calculator();
		this.myHistory = new History();
	}
	
	@Test
	public void Testaddition()
	{
		
		int a,b;
		a=1;
		b=3;
		
		assert(maCal.Add(a, b)==a+b);
	}
	
	@Test
	public void MultiplyTest() 
	{
		float a,b;
		a=(float)1.345;
		b=(float)342.798;
		
		assert(maCal.Multiply(a, b)==a*b);

	}
	
	
	@Test
	public void DivideTest() 
	{
		float a,b;
		a=(float)1.345;
		b=(float)342.798;
		float result = a/b;
		
		assert(maCal.Divide(a, b)==a/b);

	}
	
	@Test
	public void factorielTest() 
	{
		int n =10;
		
		assert(maCal.factoriel(n)==3628800);

	}
	
	@Test
	public void toBinaryTest()
	{
		int n = 18;
		String nbBin = Integer.toBinaryString(n);
		
		assert(maCal.toBinary(n).equals("10010"));		
	}
	
	public void toHexTest()
	{
		int n = 28;
		String decimal = Integer.toHexString(n);
		
		assert(maCal.toHex(n).equals("1c"));
	}
	
	@Test
	public void getHistoryTest() 
	{
		Date maDate = new Date();
		String calcul = "5 + 58 = 63";
		OperationText myOp = new OperationText(calcul,maDate);
		this.myHistory.addItemList(myOp);
	
		assert( this.myHistory.getHistoryString().equals("\n5 + 58 = 63 "+maDate) );
		
		
	}
	
	@Test
	public void addCalcul()
	{
		int taille = this.myHistory.getHistoryOperation().size();
		Date maDate = new Date();
		String calcul = "5+3=8";
		OperationText myOp = new OperationText(calcul,maDate);
		this.myHistory.addItemList(myOp);
		
		assert (this.myHistory.getHistoryOperation().size()==taille+1);
		
	}
	
	
	
	
	
	
	

}
