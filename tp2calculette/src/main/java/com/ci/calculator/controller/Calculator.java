package com.ci.calculator.controller;

import com.ci.calculator.model.History;
import com.ci.calculator.model.OperationText;

public class Calculator {
	
	private History myHistory;
	
	public Calculator() 
	{
		myHistory=new History();
	}
	
	public float Divide(float a, float b) 
	{
		float result = a/b;
		return result;
	}
	
	public float Multiply(float a, float b) 
	{
		float result = a*b;
		return result;
	}
	
	public float Add(float a, float b) 
	{
		float result = a+b;
		return result;
	}
	
	public int factoriel(int n)
	{
		int facto=1;
		
		if(n==0)
		{
			return facto;
		}
		else
		{
		facto=n*factoriel(n-1);
		return facto;
		}
	}

	public String toBinary(int n)
	{
		String nbBin = Integer.toBinaryString(n);
		return nbBin;
	}
	
	public String toHex(int n)
	{
		String decimal = Integer.toHexString(n);
		return decimal;
	}

	
	public String getHistory() 
	{
		String Historique = myHistory.getHistoryString();
		return Historique;
	}
	
	public void addCalcul(OperationText myOp)
	{
		this.myHistory.addItemList(myOp);
	}
	
	

}
