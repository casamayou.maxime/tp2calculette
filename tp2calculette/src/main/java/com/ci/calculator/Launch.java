/**
 * 
 */
package com.ci.calculator;


import java.util.*;

import com.ci.calculator.controller.Calculator;
import com.ci.calculator.model.OperationText;

/**
 * @author maxime
 *
 */
public class Launch {
	//initialisation du scanner
		private static	Scanner sc = new Scanner(System.in);
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// Initialisation d'une calculettte
		Calculator maCalculatrice = new Calculator();
		
		//Demande à l'utilisateur de rentrer une operation
		
		int test=1;
		String reponse = "";
		
		while (test==1)
		{
			int typeCalcul;
			System.out.println("Voulez vous réaliser: \nUne operation simple : 1 \nUne factorielle : 2 \nUne conversion : 3" );
			typeCalcul = sc.nextInt();
			if (typeCalcul==2)
			{
				realiserUnefacto(maCalculatrice);
			}
			else if (typeCalcul==3)
			{
				realiserUneConversion(maCalculatrice);
			}
			
			else
			{
				realiserUneOperation(maCalculatrice);
			}
			System.out.println("voulez vous continuer? \nOui : O \nNon : N \nVoir l'historique? : H" );
			reponse="";
			reponse = sc.next();
			if (reponse.equalsIgnoreCase("N"))
			{
				test =0;
			}
			else if (reponse.equalsIgnoreCase("H"))
			{
				System.out.println("Voici l'historique en mémoire : \n"+maCalculatrice.getHistory());
			}
			
		}
		
		System.out.println("FIN");
		

	}
	
	public static void realiserUneOperation(Calculator myCalcu)
	{
		System.out.println("Entrez votre premier nombre :");
		float a = sc.nextFloat();
		
		System.out.println("Entrez l'opération souhaitée (+,-,x,/) :");
		String op = sc.next();

		System.out.println("Entrez votre deuxieme nombre");
		float b = sc.nextFloat();
	
		float resultat=0;
		
		
		
		if (op.equals("+"))
		{
			resultat = myCalcu.Add(a,b);
			System.out.println("Resultat = "+resultat);
		}
		else if (op.equals("-"))
		{
			resultat = myCalcu.Add(a,-b);
			System.out.println("Resultat = "+resultat);
		}
		else if (op.equals("x"))
		{
			resultat = myCalcu.Multiply(a,b);
			System.out.println("Resultat = "+resultat);
		}
		else if (op.equals("/"))
		{
			resultat = myCalcu.Divide(a,b);
			System.out.println("Resultat = "+resultat);
		}
		else if (op.equals("!"))
		{
			resultat = myCalcu.factoriel((int)a);
			System.out.println("Resultat = "+resultat);
		}
		else 
		{
			System.out.println("Operation non prise en charge merci de recommencer");
			return;
		}
		
		String calcul = a +" "+ op +" "+ b + " = "+ resultat;
		Date maDate = new Date();
		
		OperationText monOp = new OperationText(calcul,maDate);
		
		myCalcu.addCalcul(monOp);		
	}
	
	public static void realiserUnefacto(Calculator myCalcu)
	{
		System.out.println("Entrez un entier :");
		int a = sc.nextInt();
		
		int resultat = myCalcu.factoriel((int)a);
		System.out.println("Resultat = "+resultat);
		
		String calcul = a +"! " + " = "+ resultat;
		Date maDate = new Date();
		
		
		OperationText monOp = new OperationText(calcul,maDate);
		
		myCalcu.addCalcul(monOp);
		
	}
	
	public static void realiserUneConversion(Calculator myCalcu)
	{
		System.out.println("Entrez un entier :");
		int a = sc.nextInt();
		
		System.out.println("Conversion binaire: 1 \nConversion Hexadecimale: 2");
		int choix = sc.nextInt();
		String calcul="";
		if (choix==1)
		{
			String resultat = myCalcu.toBinary(a);
			System.out.println("Resultat = "+resultat);
			
			 calcul= a +" Conversion en Binaire " + " = "+ resultat;
		}
		else
		{
			String resultat = myCalcu.toHex(a);
			System.out.println("Resultat = "+resultat);
			
			calcul = a +" Conversion en Hexadecimal " + " = "+ resultat;
		}
		Date maDate = new Date();
		
		
		OperationText monOp = new OperationText(calcul,maDate);
		
		myCalcu.addCalcul(monOp);
		
		
		
	}
	

}
