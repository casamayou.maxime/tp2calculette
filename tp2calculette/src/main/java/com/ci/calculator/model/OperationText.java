package com.ci.calculator.model;

import java.util.*;

public class OperationText {
	
	private String calcul;
	private Date maDate;
	
	public OperationText(String calcul, Date maDate) {
		this.calcul = calcul;
		this.maDate = maDate;
	}
	public String getCalcul() {
		return calcul;
	}
	public void setCalcul(String calcul) {
		this.calcul = calcul;
	}
	public Date getMaDate() {
		return maDate;
	}
	public void setMaDate(Date maDate) {
		this.maDate = maDate;
	}


}
