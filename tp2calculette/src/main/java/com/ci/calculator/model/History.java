package com.ci.calculator.model;

import java.util.*;
import java.util.Map;
import com.github.cliftonlabs.json_simple.JsonObject;




public class History 
{ 
	private List<OperationText> listOperations;
	
	public History() 
	{
		listOperations = new ArrayList<OperationText>();
	}

	public String getHistoryString()
	{
		String HistoryString ="";
		for (int i=0;i<this.listOperations.size();i++)
		{
			HistoryString=HistoryString +"\n"+ this.listOperations.get(i).getCalcul()+" "+this.listOperations.get(i).getMaDate();
		}
		return HistoryString;
		
	}
	
	/*public String toJson()
	{
		
        JsonObject objetJson = new JsonObject();
        
		// String JsonString=objetJson.getString();
		 
		// return JsonString;
	}*/
	
	public List <OperationText> getHistoryOperation() 
	{
		return listOperations;
	}

	public void addItemList( OperationText monOp) 
	{
		this.listOperations.add(monOp);
	}


}
